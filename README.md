# 02-05-resume



## Getting started
이 프로젝트는 엘리스 AI 트랙 7기 과제입니다.

## License
콘텐츠는 개인 정보이고, 프로젝트 UI 기반은 엘리스 과제 내용에 대한 클론 코딩이므로, 저작권 문제 시 비공개 및 삭제

## Project status
- UI 완료
- UX 추가 예정
